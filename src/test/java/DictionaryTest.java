import com.mmtr.spring.task22.component.Dictionary;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class DictionaryTest {

    @Test
    public void test() throws IOException {
        File file = new File("dictionary1.dat");
        String regex = "^(?<key>\\d{5})$";
        Dictionary dictionary = new Dictionary(file, regex);
        dictionary.read();
        System.out.println(dictionary.search("home"));
        dictionary.add("12345", "12345");
        System.out.println(dictionary.search("12345"));
    }
}