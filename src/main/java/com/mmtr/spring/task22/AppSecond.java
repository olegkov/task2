package com.mmtr.spring.task22;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppSecond {
    public static void main(String[] args) {
        new ClassPathXmlApplicationContext("applicationContext.xml");
    }
}
