package com.mmtr.spring.task22.service;

import com.mmtr.spring.task22.component.Dictionary;

public interface IService {
    void showDictionaries();
    void remove(String key);
    String search(String key);
    void add(String key, String value);
    Dictionary chooseDictionary();
    void showRecords();
}
